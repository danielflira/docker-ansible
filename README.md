# docker-ansible

Docker image with ansible, with this image you can generate ssh keys, copy public key to remote host and run ansible commands.

# Using

Generate ssh keys

```
$ docker run --rm -ti \
  -v ${PWD}/keys:/root/.ssh \
  danielflira/ansible \
  ssh-keygen -t rsa
```

Copying public key to host

```
$ docker run --rm -ti \
  -v ${PWD}/keys:/root/.ssh \
  danielflira/ansible \
  ssh-copy-id user@hostname
```

Running ansible playbooks

```
$ docker run --rm -ti \
  -v ${PWD}/keys:/root/.ssh \
  -v ${PWD}/playbooks:/playbooks \
  danielflira/ansible \
  ansible-playbook -i user@hostname, /playbooks/task1.yaml
```

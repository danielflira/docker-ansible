FROM debian

RUN apt-get update && apt-get install -y \
    python3-minimal \
    python3-pip \
    ssh

RUN pip3 install ansible
